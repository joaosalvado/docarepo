﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net.Http;
using System.Threading.Tasks;

namespace WebApplication18.Controllers
{
    public class HomeController : Controller
    {

    public async Task LoadSomeData()
        {
            int delay = 3;

            if (Request.QueryString["t"] != null)
            {
                Int32.TryParse(Request.QueryString["t"].ToString(), out delay);
            }

            HttpClient Client = new HttpClient();

            Client.DefaultRequestHeaders.Add("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.162 Safari/537.36");
            Client.DefaultRequestHeaders.Add("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8");

            var clientcontacts = Client.GetStringAsync("https://httpbin.org/delay/" + delay);

            await Task.WhenAll(clientcontacts);

            //message.Text = clientcontacts.Result;
        }
    

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Error()
        {
            ViewBag.Message = "Your application description page.";

            throw new Exception();

            return View();
        }

        public async Task<ActionResult>  Delay()
        {
            ViewBag.Message = "Your contact page.";

            await LoadSomeData();

            return View();
        }
    }
}